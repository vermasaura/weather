import XCTest
@testable import Weather

protocol CurrentWeatherControllerTestsScenarios {
    func testWeather_happy_days() throws
    func testWeather_failure_parsing() throws
    func testWeather_failure_empty_list() throws
    func testWeather_failure() throws
}

final class CurrentWeatherControllerTests: XCTestCase {

    private var mockHTTPClient: MockHTTPClient!
    private var controller: CurrentWeatherControlling!
    
    override func setUpWithError() throws {
        
        mockHTTPClient = MockHTTPClient()
        controller = CurrentWeatherController(httpClient: mockHTTPClient)
    }

    override func tearDownWithError() throws {
        mockHTTPClient = nil
        controller = nil
    }
}

// MARK: - Tests

extension CurrentWeatherControllerTests: CurrentWeatherControllerTestsScenarios {
    
    func testWeather_happy_days() throws {
        
        // Given
        var result: Result<CurrentWeatherResponse, WeatherError>?
        let cities = ["12345", "67890"]
        
        // When
        controller.weather(
            for: cities
        ) {
            result = $0
        }
        
        guard
            let url = Bundle.main.url(forResource: "weather_happydays", withExtension: "json"),
            let data = try? Data(contentsOf: url) else {
            XCTFail("Failed to load json file")
            return
        }
        
        mockHTTPClient.requestResponse?(.success(data))
        
        // Then
        XCTAssertEqual(mockHTTPClient.calls, [.request])
        XCTAssertEqual(
            mockHTTPClient.requestURLRequest?.url?.absoluteString,
            "https://api.openweathermap.org/data/2.5/group?id=12345,67890&mode=json&units=metric&APPID=e604812dfc166c0f2646d292c5441bf7")
        XCTAssertNotNil(mockHTTPClient.requestResponse)
        let response = try XCTUnwrap(result?.value)
        XCTAssertEqual(response.list?.count, 3)
    }
    
    func testWeather_failure_parsing() throws {
        
        // Given
        var result: Result<CurrentWeatherResponse, WeatherError>?
        let cities = ["12345", "67890"]
        
        // When
        controller.weather(
            for: cities
        ) {
            result = $0
        }
        
        guard
            let url = Bundle.main.url(forResource: "weather_failure_parse", withExtension: "json"),
            let data = try? Data(contentsOf: url) else {
            XCTFail("Failed to load json file")
            return
        }
        
        mockHTTPClient.requestResponse?(.success(data))
        
        // Then
        XCTAssertEqual(mockHTTPClient.calls, [.request])
        XCTAssertEqual(
            mockHTTPClient.requestURLRequest?.url?.absoluteString,
            "https://api.openweathermap.org/data/2.5/group?id=12345,67890&mode=json&units=metric&APPID=e604812dfc166c0f2646d292c5441bf7")
        XCTAssertNotNil(mockHTTPClient.requestResponse)
        let error = try XCTUnwrap(result?.error)
        XCTAssertEqual(error, .custom(errorDescription: "Failed to parse JSON"))
    }
    
    func testWeather_failure_empty_list() throws {
        
        // Given
        var result: Result<CurrentWeatherResponse, WeatherError>?
        let cities = ["12345", "67890"]
        
        // When
        controller.weather(
            for: cities
        ) {
            result = $0
        }
        
        guard
            let url = Bundle.main.url(forResource: "weather_failure_emptyList", withExtension: "json"),
            let data = try? Data(contentsOf: url) else {
            XCTFail("Failed to load json file")
            return
        }
        
        mockHTTPClient.requestResponse?(.success(data))
        
        // Then
        XCTAssertEqual(mockHTTPClient.calls, [.request])
        XCTAssertEqual(
            mockHTTPClient.requestURLRequest?.url?.absoluteString,
            "https://api.openweathermap.org/data/2.5/group?id=12345,67890&mode=json&units=metric&APPID=e604812dfc166c0f2646d292c5441bf7")
        XCTAssertNotNil(mockHTTPClient.requestResponse)
        let error = try XCTUnwrap(result?.error)
        XCTAssertEqual(error, .emptyList)
    }
    
    func testWeather_failure() throws {
        
        // Given
        var result: Result<CurrentWeatherResponse, WeatherError>?
        let cities = ["12345", "67890"]
        
        // When
        controller.weather(
            for: cities
        ) {
            result = $0
        }
        
        func assert(with weatherError: WeatherError) throws {
            // Then
            XCTAssertEqual(mockHTTPClient.calls, [.request])
            XCTAssertEqual(
                mockHTTPClient.requestURLRequest?.url?.absoluteString,
                "https://api.openweathermap.org/data/2.5/group?id=12345,67890&mode=json&units=metric&APPID=e604812dfc166c0f2646d292c5441bf7")
            XCTAssertNotNil(mockHTTPClient.requestResponse)
            let error = try XCTUnwrap(result?.error)
            XCTAssertEqual(error, weatherError)
        }
        
        func testWeather_failureClient() {
            mockHTTPClient.requestResponse?(.failure(.client(statusCode: 400, data: nil)))
            try? assert(with: .custom(errorDescription: "Server rejected the request"))
        }
        
        func testWeather_failureServer() {
            mockHTTPClient.requestResponse?(.failure(.server(statusCode: 500, data: nil)))
            try? assert(with: .custom(errorDescription: "Server Failure"))
        }
        
        func testWeather_failureNetwork() {
            mockHTTPClient.requestResponse?(.failure(.noNetwork))
            try? assert(with: .network)
        }
        
        func testWeather_failureCancelled() {
            mockHTTPClient.requestResponse?(.failure(.cancelled))
            try? assert(with: .custom(errorDescription: "Request was cancelled"))
        }
        
        func testWeather_failureRequest() {
            mockHTTPClient.requestResponse?(.failure(.request))
            try? assert(with: .custom(errorDescription: "Bad Request"))
        }
        
        func testWeather_failureUnknown() {
            mockHTTPClient.requestResponse?(.failure(.unknown))
            try? assert(with: .custom(errorDescription: "Unkown error has occured"))
        }
        
        testWeather_failureClient()
        testWeather_failureServer()
        testWeather_failureNetwork()
        testWeather_failureCancelled()
        testWeather_failureRequest()
        testWeather_failureUnknown()
    }
}
