import XCTest
@testable import Weather

protocol WeatherFeedViewModelTestsDisplayScenarios {
    func testDisplayDidAppear_display_calls()
    func testDisplayDidAppear_display_screen_title()
    func testDisplayDidAppear_display_refresh_timer()
    func testDisplayDidAppear_display_add_button()
}

protocol WeatherFeedViewModelTestsActionsScenarios {
    func testDisplayDidAppear_display_refresh_timer_action()
    func testDisplayDidAppear_display_add_button_action()
    func testDisplayDidAppear_user_added_new_city_action()
    func testDisplayDidAppear_user_added_duplicate_city_action()
}

protocol WeatherFeedViewModelTestsFetchWeatherScenarios {
    func testFetchWeather_happy_days()
    func testFetchWeather_cell_action()
    func testFetchWeather_failure_empty_list()
    func testFetchWeather_failure_network()
    func testFetchWeather_failure_custom()
}

final class WeatherFeedViewModelTests: XCTestCase {

    private var mockRouter: MockWeatherFeedRouter!
    private var mockController: MockCurrentWeatherController!
    private var mockBuilder: MockWeatherFeedBuilder!
    private var mockBuilderProvider: (() -> WeatherFeedBuilding)!
    private var mockDisplay: MockWeatherFeedDisplaying!
    private var viewModel: WeatherFeedViewModel!
    
    override func setUpWithError() throws {
        mockRouter = MockWeatherFeedRouter()
        mockController = MockCurrentWeatherController()
        mockBuilder = MockWeatherFeedBuilder()
        mockBuilderProvider = { self.mockBuilder }
        mockDisplay = MockWeatherFeedDisplaying()
        
        viewModel = WeatherFeedViewModel(
            controller: mockController,
            builderProvider: mockBuilderProvider,
            router: mockRouter
        )
        viewModel.display = mockDisplay
    }

    override func tearDownWithError() throws {
        mockRouter = nil
        mockController = nil
        mockBuilder = nil
        mockBuilderProvider = nil
        mockDisplay = nil
        viewModel = nil
    }
}

// MARK: - Tests

// MARK: Tests related to Display

extension WeatherFeedViewModelTests: WeatherFeedViewModelTestsDisplayScenarios {
    func testDisplayDidAppear_display_calls() {
        
        // When
        viewModel.displayDidAppear()
        
        // Then
        XCTAssertEqual(mockDisplay.calls, [.setTitle, .setRefreshItem, .setRightBarButtonItem])
    }
    
    func testDisplayDidAppear_display_screen_title() {
        
        // When
        viewModel.displayDidAppear()
        
        // Then
        XCTAssertNotNil(mockDisplay.setTitle)
        XCTAssertEqual(mockDisplay.setTitle, "openweathermap.org")
    }
    
    func testDisplayDidAppear_display_refresh_timer() {
        
        // When
        viewModel.displayDidAppear()
        
        // Then
        XCTAssertNotNil(mockDisplay.setRefreshItem)
        XCTAssertEqual(mockDisplay.setRefreshItem?.refreshInterval, 30.0)
        XCTAssertEqual(mockDisplay.setRefreshItem?.shouldRepeat, true)
        XCTAssertNotNil(mockDisplay.setRefreshItem?.action)
    }
    
    func testDisplayDidAppear_display_add_button() {
        
        // When
        viewModel.displayDidAppear()
        
        // Then
        XCTAssertNotNil(mockDisplay.setRightBarButtonItem)
        XCTAssertEqual(mockDisplay.setRightBarButtonItem?.systemItem, .add)
        XCTAssertNotNil(mockDisplay.setRightBarButtonItem?.action)
    }
}

// MARK: Actions

extension WeatherFeedViewModelTests: WeatherFeedViewModelTestsActionsScenarios {
    
    // MARK: Timer Action
    
    func testDisplayDidAppear_display_refresh_timer_action() {
        
        // When
        viewModel.displayDidAppear()
        mockDisplay.setRefreshItem?.action()
        
        // Then
        XCTAssertEqual(mockController.calls, [.weather, .weather])
        XCTAssertEqual(mockController.weatherCities?.count, 3)
        XCTAssertNotNil(mockController.weatherResponse)
    }
    
    // MARK: Add button Action
    
    func testDisplayDidAppear_display_add_button_action() {
        
        // When
        viewModel.displayDidAppear()
        mockDisplay.setRightBarButtonItem?.action()
        
        // Then
        XCTAssertEqual(mockRouter.calls, [.showAddCityScreen])
        XCTAssertNotNil(mockRouter.showAddCityScreenAddCityHandler)
    }
    
    // MARK: City added handled Action
    
    func testDisplayDidAppear_user_added_new_city_action() {
        
        // When
        viewModel.displayDidAppear()
        mockDisplay.setRightBarButtonItem?.action()
        mockRouter.showAddCityScreenAddCityHandler?(33333)
        
        // Then
        XCTAssertEqual(mockController.calls, [.weather, .weather])
        XCTAssertEqual(mockController.weatherCities?.count, 4)
        XCTAssertNotNil(mockController.weatherResponse)
    }
    
    func testDisplayDidAppear_user_added_duplicate_city_action() {
        
        // When
        viewModel.displayDidAppear()
        mockDisplay.setRightBarButtonItem?.action()
        mockRouter.showAddCityScreenAddCityHandler?(4163971)
        
        // Then
        XCTAssertEqual(mockController.calls, [.weather, .weather])
        XCTAssertEqual(mockController.weatherCities?.count, 3)
        XCTAssertNotNil(mockController.weatherResponse)
    }
}

// MARK: Fetch Weather

extension WeatherFeedViewModelTests: WeatherFeedViewModelTestsFetchWeatherScenarios {
    
    // MARK: Happy days
    
    func testFetchWeather_happy_days() {
        // Given
        let response: CurrentWeatherResponse = .mock()
        
        // When
        viewModel.displayDidAppear()
        mockController.weatherResponse?(.success(response))
        
        // Then
        
        // Controller Calls
        XCTAssertEqual(mockController.calls, [.weather])
        XCTAssertEqual(mockController.weatherCities?.count, 3)
        XCTAssertNotNil(mockController.weatherResponse)
        
        // Builder Calls
        XCTAssertEqual(mockBuilder.calls, [.addWeatherFeed, .build])
        XCTAssertEqual(mockBuilder.addWeatherFeed, response)
        XCTAssertNotNil(mockBuilder.addWeatherFeedAction)
        
        // Display Calls
        XCTAssertEqual(mockDisplay.calls, [.setTitle, .setRefreshItem, .setRightBarButtonItem, .setSections])
        XCTAssertNotNil(mockDisplay.setSections)
    }
    
    func testFetchWeather_cell_action() {
        // Given
        let response: CurrentWeatherResponse = .mock()
        
        // When
        viewModel.displayDidAppear()
        mockController.weatherResponse?(.success(response))
        mockBuilder.addWeatherFeedAction?(response.list!.first!)()
        
        // Then
        XCTAssertEqual(mockRouter.calls, [.showWeatherDetailScreen])
        XCTAssertEqual(mockRouter.showWeatherDetailScreenWeather, response.list!.first!)
    }
    
    // MARK: Failure tests
    
    func testFetchWeather_failure_empty_list() {
        
        //When
        viewModel.displayDidAppear()
        mockController.weatherResponse?(.failure(.emptyList))
        
        //Then
        
        // Builder Calls
        XCTAssertEqual(mockBuilder.calls, [.addErrorView, .build])
        XCTAssertEqual(mockBuilder.addErrorViewErrorText, "There is no weather information at this moment.")
        
        // Display Calls
        XCTAssertEqual(mockDisplay.calls, [.setTitle, .setRefreshItem, .setRightBarButtonItem, .setSections])
        XCTAssertNotNil(mockDisplay.setSections)
    }
    
    func testFetchWeather_failure_network() {
        
        //When
        viewModel.displayDidAppear()
        mockController.weatherResponse?(.failure(.network))
        
        //Then
        
        // Builder Calls
        XCTAssertEqual(mockBuilder.calls, [.addErrorView, .build])
        XCTAssertEqual(mockBuilder.addErrorViewErrorText, "Check your internet connection and try again!")
        
        // Display Calls
        XCTAssertEqual(mockDisplay.calls, [.setTitle, .setRefreshItem, .setRightBarButtonItem, .setSections])
        XCTAssertNotNil(mockDisplay.setSections)
    }
    
    func testFetchWeather_failure_custom() {
        
        //When
        viewModel.displayDidAppear()
        mockController.weatherResponse?(.failure(.custom(errorDescription: "Custom error")))
        
        //Then
        
        // Builder Calls
        XCTAssertEqual(mockBuilder.calls, [.addErrorView, .build])
        XCTAssertEqual(mockBuilder.addErrorViewErrorText, "Custom error")
        
        // Display Calls
        XCTAssertEqual(mockDisplay.calls, [.setTitle, .setRefreshItem, .setRightBarButtonItem, .setSections])
        XCTAssertNotNil(mockDisplay.setSections)
    }
}

// MARK: Mock WeatherFeedDisplaying

private class MockWeatherFeedDisplaying: WeatherFeedDisplaying {
    
    enum FunctionCall {
        case setTitle
        case setRightBarButtonItem
        case setRefreshItem
        case setSections
    }
    
    private(set) var calls: [FunctionCall] = []
    
    private(set) var setTitle: String?
    func set(title: String?) {
        setTitle = title
        calls.append(.setTitle)
    }
    
    private(set) var setRightBarButtonItem: NavigationButtonItem?
    func set(rightBarButtonItem: NavigationButtonItem) {
        setRightBarButtonItem = rightBarButtonItem
        calls.append(.setRightBarButtonItem)
    }
    
    private(set) var setRefreshItem: WeatherRefreshItem?
    func set(refreshItem: WeatherRefreshItem) {
        setRefreshItem = refreshItem
        calls.append(.setRefreshItem)
    }
    
    private(set) var setSections: [TableViewSectionItem]?
    func set(sections: [TableViewSectionItem]) {
        setSections = sections
        calls.append(.setSections)
    }
}
