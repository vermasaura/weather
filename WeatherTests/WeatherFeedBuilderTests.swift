import XCTest
@testable import Weather

protocol WeatherFeedBuilderTestsScenarios {
    func testAddWeatherFeed() throws
    func testAddWeatherFeed_empty_list() throws
    func testAddErrorView() throws
}

final class WeatherFeedBuilderTests: XCTestCase {
    
    private var builder: WeatherFeedBuilding!
        
    override func setUpWithError() throws {
        builder = WeatherFeedBuilder()
    }

    override func tearDownWithError() throws {
        builder = nil
    }
}

// MARK: - Tests

extension WeatherFeedBuilderTests: WeatherFeedBuilderTestsScenarios {
    
    func testAddWeatherFeed() throws {
        
        // Given
        var actionCalled = false
        let action: ((Weather) -> (() -> Void)) = { weather in
            return {
                actionCalled = true
            }
        }
        
        // When
        let sections = builder
            .addWeatherFeed(.mock(), action: action)
            .build()
        
        let header = try? XCTUnwrap(sections.first?.header as? WeatherFeedHeaderFooterItem)
        let items = try? XCTUnwrap(sections.first?.items as? [WeatherFeedViewItem])
        items?.first?.action?()
        
        // Count
        XCTAssertEqual(sections.count, 1)
        XCTAssertEqual(items?.count, 1)
        
        // Header
        XCTAssertEqual(header?.title, "Weather Today")
        
        // Footer
        XCTAssertNil(sections.first?.footer)
        
        // Items
        XCTAssertEqual(items?.first?.cityText, "Melbourne")
        XCTAssertEqual(items?.first?.temperatureText, "27.5°")
        XCTAssertEqual(items?.first?.temperatureRange, "22.3° - 28.9°")
        XCTAssertEqual(items?.first?.hasDetails, true)
        XCTAssertNotNil(items?.first?.action)
        XCTAssertEqual(actionCalled, true)
    }
    
    func testAddWeatherFeed_empty_list() throws {
                
        let action: ((Weather) -> (() -> Void)) = { weather in
            return {}
        }
        
        // When
        let sections = builder
            .addWeatherFeed(CurrentWeatherResponse(list: nil), action: action)
            .build()
        
        // Count
        XCTAssertEqual(sections.count, 0)
    }
    
    func testAddErrorView() throws {
        
        // When
        let sections = builder
            .addErrorView("Error text")
            .build()
        
        let items = try? XCTUnwrap(sections.first?.items as? [ListErrorViewItem])
        
        // Then
        
        // Count
        XCTAssertEqual(sections.count, 1)
        XCTAssertEqual(items?.count, 1)
        
        // Header and Footer
        XCTAssertNil(sections.first?.header)
        XCTAssertNil(sections.first?.footer)
        
        // Items
        XCTAssertEqual(items?.first?.errorText, "Error text")
    }
}
