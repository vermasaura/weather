import Foundation
@testable import Weather

final class MockHTTPClient: HTTPClientType {
    
    enum FunctionCall {
        case request
    }
    
    private(set) var calls: [FunctionCall] = []
    
    private(set) var requestURLRequest: URLRequest?
    private(set) var requestResponse: ((Result<HTTPResponse, HTTPError>) -> Void)?
    func request(
        with urlRequest: URLRequest,
        response: @escaping (Result<HTTPResponse, HTTPError>) -> Void
    ) {
        requestURLRequest = urlRequest
        requestResponse = response
        calls.append(.request)
    }
}
