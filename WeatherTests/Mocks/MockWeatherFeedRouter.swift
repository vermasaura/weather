import Foundation
@testable import Weather

final class MockWeatherFeedRouter: WeatherFeedRouting {
    
    enum FunctionCall {
        case showAddCityScreen
        case showWeatherDetailScreen
    }
    
    private(set) var calls: [FunctionCall] = []
    
    private(set) var showAddCityScreenAddCityHandler: ((CityIdentifier) -> Void)?
    func showAddCityScreen(addCityHandler: @escaping (CityIdentifier) -> Void) {
        showAddCityScreenAddCityHandler = addCityHandler
        calls.append(.showAddCityScreen)
    }
    
    private(set) var showWeatherDetailScreenWeather: Weather?
    func showWeatherDetailScreen(with weather: Weather) {
        showWeatherDetailScreenWeather = weather
        calls.append(.showWeatherDetailScreen)
    }
}
