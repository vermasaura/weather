import Foundation
@testable import Weather

final class MockCurrentWeatherController: CurrentWeatherControlling {
    
    enum FunctionCall {
        case weather
    }
    
    private(set) var calls: [FunctionCall] = []
    
    private(set) var weatherCities: [String]?
    private(set) var weatherResponse: ((Result<CurrentWeatherResponse, WeatherError>) -> Void)?
    func weather(
        for cities: [String],
        response: @escaping (Result<CurrentWeatherResponse, WeatherError>) -> Void
    ) {
        weatherCities = cities
        weatherResponse = response
        calls.append(.weather)
    }
}
