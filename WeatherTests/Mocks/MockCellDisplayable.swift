import Foundation
import UIKit
@testable import Weather

final class MockCellDisplayable: CellDisplayable {
    var action: (() -> Void)?
    
    func extractCell(from tableView: UITableView, for indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
}
