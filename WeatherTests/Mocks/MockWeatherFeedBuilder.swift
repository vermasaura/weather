import Foundation
@testable import Weather

final class MockWeatherFeedBuilder: WeatherFeedBuilding {
    
    enum FunctionCall {
        case build
        case addWeatherFeed
        case addErrorView
    }
    
    private(set) var calls: [FunctionCall] = []
    
    func build() -> [TableViewSectionItem] {
        calls.append(.build)
        return [TableViewSectionItem(items: [MockCellDisplayable()])]
    }
    
    private(set) var addWeatherFeed: CurrentWeatherResponse?
    private(set) var addWeatherFeedAction: ((Weather) -> (() -> Void))?
    func addWeatherFeed(_ feed: CurrentWeatherResponse, action: @escaping (Weather) -> (() -> Void)) -> Self {
        addWeatherFeed = feed
        addWeatherFeedAction = action
        calls.append(.addWeatherFeed)
        return self
    }
    
    private(set) var addErrorViewErrorText: String?
    func addErrorView(_ errorText: String) -> Self {
        addErrorViewErrorText = errorText
        calls.append(.addErrorView)
        return self
    }
}
