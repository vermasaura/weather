import Foundation
@testable import Weather

extension CurrentWeatherResponse {
    static func mock() -> CurrentWeatherResponse {
        return CurrentWeatherResponse(list: [.mock()])
    }
}

extension Weather {
    static func mock() -> Weather {
        return Weather(
            id: 1234,
            name: "Melbourne",
            main: .mock()
        )
    }
}

extension Weather.Main {
    static func mock() -> Weather.Main {
        return Weather.Main(
            temperature: 27.5,
            feelsLike: 25.4,
            temperatureMin: 22.3,
            temperatureMax: 28.9,
            humidity: 60.5
        )
    }
}
