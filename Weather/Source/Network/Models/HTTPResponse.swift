import Foundation

enum HTTPError: Error, Equatable {
    
    case client(statusCode: Int, data: Data?) //400 ... 499,
    case server(statusCode: Int, data: Data?) //500 ... 599,
    case noNetwork
    case request
    case cancelled
    case unknown
}

typealias HTTPResponse = Data
