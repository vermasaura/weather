import Foundation

protocol HTTPClientType {
    
    func request(
        with urlRequest: URLRequest,
        response: @escaping (Result<HTTPResponse, HTTPError>) -> Void
    )
}

final class HTTPClient {
 
    // MARK:- Initialisation
    
    init(
        session: SessionProtocol = URLSession.shared
    ) {
        self.session = session
    }
    
    // MARK: Private
    
    // MARK: Properties
    
    private let session: SessionProtocol
}

// MARK: - Conformance

// MARK: HTTPClientType

extension HTTPClient: HTTPClientType {
    
    func request(
        with urlRequest: URLRequest,
        response: @escaping (Result<HTTPResponse, HTTPError>) -> Void
    ) {
        session.dataTask(with: urlRequest) {
            [weak self] (data, urlResponse, error) in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                response(self.processResult(
                    data: data,
                    urlResponse: urlResponse,
                    error: error
                ))
            }
        }.resume()
    }
}

// MARK:- Helpers

private extension HTTPClient {

    func processResult(
        data: Data?,
        urlResponse: URLResponse?,
        error: Error?
    ) -> (Result<HTTPResponse, HTTPError>) {
    
        if let error = error as? URLError {
            switch error.code {
            case .notConnectedToInternet:
                return .failure(.noNetwork)
                
            case .cancelled:
                return .failure(.cancelled)
                
            default:
                return .failure(.request)
            }
        } else if let httpResponse = urlResponse as? HTTPURLResponse {
            switch httpResponse.statusCode {
            case 200 ... 399:
                return self.parseSuccess(data: data)
                
            case 400 ... 499:
                return .failure(.client(statusCode: httpResponse.statusCode, data: data))
                
            case 500 ... 599:
                return .failure(.server(statusCode: httpResponse.statusCode, data: data))
                
            default:
                return .failure(.unknown)
            }
            
        } else {
            return .failure(.unknown)
        }
    }
    
    func parseSuccess(
        data: Data?
    ) -> (Result<HTTPResponse, HTTPError>) {
        
        guard let data = data else {
            return .failure(.unknown)
        }

        return .success(data)
    }
}
