import Foundation

struct CurrentWeatherResponse: Decodable, Equatable {
    let list: [Weather]?
}

struct Weather: Decodable, Equatable {
    
    struct Main: Codable, Equatable {
        let temperature: Float
        let feelsLike: Float
        let temperatureMin: Float
        let temperatureMax: Float
        let humidity: Float
        
        private enum CodingKeys : String, CodingKey {
            case temperature = "temp"
            case feelsLike = "feels_like"
            case temperatureMin = "temp_min"
            case temperatureMax = "temp_max"
            case humidity
        }
    }
    
    let id: UInt64
    let name: String?
    let main: Main?
}

enum WeatherError: Error, Equatable {
    case emptyList
    case network
    case custom(errorDescription: String)
}
