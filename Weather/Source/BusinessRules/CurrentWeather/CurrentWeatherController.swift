import Foundation

protocol CurrentWeatherControlling {
    func weather(
        for cities: [String],
        response: @escaping (Result<CurrentWeatherResponse, WeatherError>) -> Void
    )
}

final class CurrentWeatherController {
    
    // MARK:- Initialising
    
    init(
        httpClient: HTTPClientType = HTTPClient()
    ) {
        self.httpClient = httpClient
    }
    
    // MARK: - Private
    
    // MARK: Properties
    
    private let httpClient: HTTPClientType
}

// MARK: - Conformance

// MARK: CurrentWeatherControlling

extension CurrentWeatherController: CurrentWeatherControlling {
    func weather(
        for cities: [String],
        response: @escaping (Result<CurrentWeatherResponse, WeatherError>) -> Void
    ) {
     
        guard let url = currentWeatherComponents(for: cities).url else {
            return response(.failure(WeatherError.custom(errorDescription: "Bad Request")))
        }
        
        httpClient.request(
            with: URLRequest(url: url)
        ) { [weak self] result in
            
            guard let self = self else { return }
            
            switch result {
            case .success(let httpResponse):
                return self.handleSuccess(with: httpResponse, response: response)
            case .failure(let httpError):
                return self.handleFailure(with: httpError, response: response)
            }
        }
    }
}

// MARK: - Helpers

private extension CurrentWeatherController {
    func handleSuccess(
        with httpResponse: HTTPResponse,
        response: @escaping (Result<CurrentWeatherResponse, WeatherError>) -> Void
    ) {
        guard
            let decoded = try? JSONDecoder().decode(CurrentWeatherResponse.self, from: httpResponse) else {
            return response(.failure(.custom(errorDescription: "Failed to parse JSON")))
        }
        
        guard let list = decoded.list, list.count > 0  else {
            return response(.failure(.emptyList))
        }
        
        response(.success(decoded))
    }
    
    func handleFailure(
        with httpError: HTTPError,
        response: @escaping (Result<CurrentWeatherResponse, WeatherError>) -> Void
    ) {
        switch httpError {
        case .client:
            response(.failure(.custom(errorDescription: "Server rejected the request")))
        case .noNetwork:
            response(.failure(.network))
        case .cancelled:
            response(.failure(.custom(errorDescription: "Request was cancelled")))
        case .server:
            response(.failure(.custom(errorDescription: "Server Failure")))
        case .request:
            response(.failure(.custom(errorDescription: "Bad Request")))
        default:
            response(.failure(.custom(errorDescription: "Unkown error has occured")))
        }
    }
}

// MARK: - Constants

private extension CurrentWeatherController {
  struct API {
    static let scheme = "https"
    static let host = "api.openweathermap.org"
    static let path = "/data/2.5"
    static let key = "e604812dfc166c0f2646d292c5441bf7"
  }
  
  func currentWeatherComponents(
    for cities: [String]
  ) -> URLComponents {
    var components = URLComponents()
    components.scheme = API.scheme
    components.host = API.host
    components.path = API.path + "/group"
    
    components.queryItems = [
        URLQueryItem(name: "id", value: cities.joined(separator: ",")),
      URLQueryItem(name: "mode", value: "json"),
      URLQueryItem(name: "units", value: "metric"),
      URLQueryItem(name: "APPID", value: API.key)
    ]
    
    return components
  }
}

