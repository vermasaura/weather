import Foundation

struct City: Decodable {
    let id: UInt
    let name: String
    let country: String
}

enum SearchCityError: Error {
    case emptyList
    case custom(errorDescription: String)
}
