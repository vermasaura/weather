import Foundation

protocol SearchCityControlling {
    
    func openDatabase(response: @escaping () -> Void)
    func cities(
        by searchText: String,
        response: @escaping (Result<[City], SearchCityError>) -> Void
    )
}

final class SearchCityController {
    
    // MARK: - Initialising
    
    init() {}
    
    // MARK: - Private
    
    // MARK: Properties
    
    private var cities: [City] = []
}

// MARK: - Helpers

private extension SearchCityController {
    func loadJson(_ fileName: String) {
        
        
    }
}

// MARK: - Conformance

// MARK: SearchCityControlling

extension SearchCityController: SearchCityControlling {
    
    func openDatabase(response: @escaping () -> Void) {
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard
                let self = self,
                let url = Bundle.main.url(forResource: CityDatabase.fileName, withExtension: "json"),
                let data = try? Data(contentsOf: url),
                let decoding = try? JSONDecoder().decode([City].self, from: data)
            else {
                return
            }
            
            self.cities = decoding
            DispatchQueue.main.async {
                response()
            }
        }
    }
    
    func cities(
        by searchText: String,
        response: @escaping (Result<[City], SearchCityError>) -> Void
    ) {
        
        guard cities.count > 0 else {
            return response(.failure(.custom(errorDescription: "Failed to read the city database")))
        }
        
        let filteredCities = cities.filter {
            $0.name.contains(searchText)
        }
        
        guard filteredCities.count > 0 else {
            return response(.failure(.emptyList))
        }
        
        response(.success(filteredCities))
    }
}

// MARK: - Constants

private extension SearchCityController {
  struct CityDatabase {
    static let fileName = "city_list"
  }
}
