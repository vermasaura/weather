import Foundation
import UIKit

struct ListErrorViewItem {
    let errorText: String
}

extension ListErrorViewItem: CellDisplayable {
    func extractCell(from tableView: UITableView, for indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ListErrorCell.identifier) as? ListErrorCell else {
            return UITableViewCell()
        }
        cell.configure(with: self)
        return cell
    }
}
