import Foundation
import UIKit

struct TableViewSectionItem {

    // MARK:- Properties
    
    let header: HeaderFooterDisplayable?
    let items: [CellDisplayable]
    let footer: HeaderFooterDisplayable?
    
    // MARK:- Initialising
    
    init(header: HeaderFooterDisplayable? = nil,
         items: [CellDisplayable],
         footer: HeaderFooterDisplayable? = nil
    ) {
        self.header = header
        self.items = items
        self.footer = footer
    }
}

protocol HeaderFooterDisplayable {
    func extractView(from tableView: UITableView, for section: Int) -> UIView?
}

protocol CellDisplayable {
    var action: (() -> Void)? { get }
    func extractCell(from tableView: UITableView, for indexPath: IndexPath) -> UITableViewCell
}

extension CellDisplayable {
    var action: (() -> Void)? {
        return nil
    }
}
