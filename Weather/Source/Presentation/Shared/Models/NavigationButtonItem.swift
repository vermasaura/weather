import Foundation
import UIKit

struct NavigationButtonItem {
    let systemItem: UIBarButtonItem.SystemItem
    let action: () -> Void
}
