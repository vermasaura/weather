import Foundation
import UIKit

final class ListErrorCell: UITableViewCell, Reusable {
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var cellTitle: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    // MARK: - View Configuration
    
    func configure(with cellItem: ListErrorViewItem) {
        cellTitle.text = cellItem.errorText
    }
}
