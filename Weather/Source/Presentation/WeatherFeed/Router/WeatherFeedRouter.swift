import Foundation
import UIKit

protocol WeatherFeedRouting {
    func showAddCityScreen(addCityHandler: @escaping (CityIdentifier) -> Void)
    func showWeatherDetailScreen(with weather: Weather)
}

final class WeatherFeedRouter {
    
    weak var display: UINavigationController?
}

// MARK: - Conformance

// MARK: WeatherFeedRouting

extension WeatherFeedRouter: WeatherFeedRouting {
    func showWeatherDetailScreen(with weather: Weather) {
        
        let router = WeatherDetailRouter()
        let viewModel = WeatherDetailViewModel(weather: weather, router: router)
        let viewController = WeatherDetailViewController(viewModel: viewModel)
        viewModel.display = viewController
        let navigationController = UINavigationController(rootViewController: viewController)
        router.display = navigationController
        display?.pushViewController(viewController, animated: true)
    }
    
    func showAddCityScreen(addCityHandler: @escaping (CityIdentifier) -> Void) {
        
        let router = AddACityRouter(addCityHandler: addCityHandler)
        let viewModel = AddACityViewModel(router: router)
        let viewController = AddACityViewController(viewModel: viewModel)
        viewModel.display = viewController
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = .fullScreen
        router.display = navigationController
        display?.present(navigationController, animated: true)
    }
}
