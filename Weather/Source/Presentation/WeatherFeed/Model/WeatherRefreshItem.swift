import Foundation

struct WeatherRefreshItem {
    let refreshInterval: Double
    let shouldRepeat: Bool
    let action: () -> Void
}
