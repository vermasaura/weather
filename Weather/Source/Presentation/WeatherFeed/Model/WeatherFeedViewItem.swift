import Foundation
import UIKit

struct WeatherFeedViewItem {
    let cityText: String
    let temperatureText: String
    let temperatureRange: String
    let hasDetails: Bool
    let action: (() -> Void)?
    
    var accessibilityText: String {
        return "It's looking \(temperatureText) in \(cityText) today with a range of \(temperatureRange)"
    }
    
    init(cityText: String,
         temperatureText: String,
         temperatureRange: String,
         hasDetails: Bool = true,
         action: (() -> Void)?
    ) {
        self.cityText = cityText
        self.temperatureText = temperatureText
        self.temperatureRange = temperatureRange
        self.hasDetails = hasDetails
        self.action = action
    }
}

extension WeatherFeedViewItem: CellDisplayable {
    func extractCell(from tableView: UITableView, for indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WeatherFeedCell.identifier) as? WeatherFeedCell else {
            return UITableViewCell()
        }
        cell.configure(with: self)
        return cell
    }
}


