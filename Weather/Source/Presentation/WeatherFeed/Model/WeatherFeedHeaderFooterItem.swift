import Foundation
import UIKit

struct WeatherFeedHeaderFooterItem {
    let title: String
}

extension WeatherFeedHeaderFooterItem: HeaderFooterDisplayable {
    func extractView(from tableView: UITableView, for section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: WeatherFeedHeaderView.identifier) as? WeatherFeedHeaderView else {
            return nil
        }
        
        headerView.configure(with: self)
        return headerView
    }
}
