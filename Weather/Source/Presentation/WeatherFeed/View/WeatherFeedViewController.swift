import Foundation
import UIKit

final class WeatherFeedViewController: UIViewController {
    
    // MARK: IBOutlet
    
    @IBOutlet private var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: String(describing: WeatherFeedCell.self),
                                     bundle: Bundle.main),
                               forCellReuseIdentifier: String(describing: WeatherFeedCell.self))
            tableView.register(UINib(nibName: String(describing: ListErrorCell.self),
                                     bundle: Bundle.main),
                               forCellReuseIdentifier: String(describing: ListErrorCell.self))
            tableView.register(UINib(nibName: String(describing: WeatherFeedHeaderView.self),
                                     bundle: Bundle.main),
                               forHeaderFooterViewReuseIdentifier: String(describing: WeatherFeedHeaderView.self))
        }
    }
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Initialisation
    
    init(
        viewModel: WeatherFeedViewModelType
    ) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: WeatherFeedViewController.self), bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("use init(viewModel:) instead")
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        activityIndicator.startAnimating()
        viewModel.displayDidAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        refreshTimer?.invalidate()
    }
    
    // MARK: Private
    
    // MARK: Properties
    
    private let viewModel: WeatherFeedViewModelType
    private var sections: [TableViewSectionItem] = []
    private var refreshTimer: Timer?
    private var rightBarButtonAction: (() -> Void)?
}

// MARK: - Helpers

private extension WeatherFeedViewController {
    
    func startTimer(with item: WeatherRefreshItem) {
        refreshTimer?.invalidate()
        refreshTimer = Timer.scheduledTimer(
            withTimeInterval: item.refreshInterval,
            repeats: item.shouldRepeat
        ) { [weak self] timer in
            
            guard let self = self else { return }
            self.activityIndicator.startAnimating()
            item.action()
        }
    }
    
    @objc func navigationBarButtonAction() {
        rightBarButtonAction?()
    }
}

// MARK: - Conformance

// MARK: UITableViewDataSource

extension WeatherFeedViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return sections[indexPath.section]
            .items[indexPath.row]
            .extractCell(from: tableView, for: indexPath)
    }
}

// MARK: UITableViewDelegate

extension WeatherFeedViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        sections[indexPath.section]
            .items[indexPath.row]
            .action?()
    }
    
    // MARK: Header Configuration
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard sections[section].header != nil else {
            return .leastNormalMagnitude
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return sections[section]
            .header?
            .extractView(from: tableView, for: section)
    }
    
    // MARK: Footer Configuration
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard sections[section].footer != nil else {
            return .leastNormalMagnitude
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return sections[section]
            .footer?
            .extractView(from: tableView, for: section)
    }
}

// MARK: WeatherFeedDisplaying

extension WeatherFeedViewController: WeatherFeedDisplaying {
    func set(title: String?) {
        self.title = title
    }
    
    func set(rightBarButtonItem: NavigationButtonItem) {
        navigationItem.setRightBarButton(
            UIBarButtonItem.init(
                barButtonSystemItem: rightBarButtonItem.systemItem,
                target: self,
                action: #selector(navigationBarButtonAction)
            ),
            animated: true
        )
        rightBarButtonAction = rightBarButtonItem.action
    }
    
    func set(refreshItem: WeatherRefreshItem) {
        startTimer(with: refreshItem)
    }

    func set(sections: [TableViewSectionItem]) {
        self.sections = sections
        activityIndicator.stopAnimating()
        tableView.reloadData()
    }
}
