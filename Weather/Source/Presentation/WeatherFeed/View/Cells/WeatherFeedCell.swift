import Foundation
import UIKit

final class WeatherFeedCell: UITableViewCell, Reusable {
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var cellTitle: UILabel!
    @IBOutlet private weak var primaryLabel: UILabel!
    @IBOutlet private weak var secondaryLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cellTitle.text = ""
        primaryLabel.text = ""
        secondaryLabel.text = ""
    }
    
    // MARK: - View Configuration
    
    func configure(with cellItem: WeatherFeedViewItem) {
        cellTitle.text = cellItem.cityText
        primaryLabel.text = cellItem.temperatureText
        secondaryLabel.text = cellItem.temperatureRange
        accessoryType = cellItem.hasDetails ? .disclosureIndicator : .none
        configureAccessibility(cellItem.accessibilityText)
    }
}

// MARK: Accessibility

private extension WeatherFeedCell {
    func configureAccessibility(_ accessibilityText: String) {
        cellTitle.isAccessibilityElement = false
        primaryLabel.isAccessibilityElement = false
        secondaryLabel.isAccessibilityElement = false
        isAccessibilityElement = true
        accessibilityLabel = accessibilityText
    }
}
