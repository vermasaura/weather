import Foundation
import UIKit

final class WeatherFeedHeaderView: UITableViewHeaderFooterView, Reusable {
    @IBOutlet private weak var headerTitle: UILabel!
    
    func configure(with item: WeatherFeedHeaderFooterItem) {
        headerTitle.text = item.title
    }
}
