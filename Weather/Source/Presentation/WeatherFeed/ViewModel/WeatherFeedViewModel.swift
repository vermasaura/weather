import Foundation

protocol WeatherFeedDisplaying: AnyObject {
    func set(title: String?)
    func set(rightBarButtonItem: NavigationButtonItem)
    func set(refreshItem: WeatherRefreshItem)
    func set(sections: [TableViewSectionItem])
}

protocol WeatherFeedViewModelType {
    func displayDidAppear()
}

final class WeatherFeedViewModel {
    
    // MARK:- Public
    weak var display: WeatherFeedDisplaying?
    
    // MARK:- Initialising
    
    init(
        controller: CurrentWeatherControlling = CurrentWeatherController(),
        builderProvider: @escaping () -> WeatherFeedBuilding = { WeatherFeedBuilder() },
        router: WeatherFeedRouting
    ) {
        self.controller = controller
        self.builderProvider = builderProvider
        self.router = router
        self.userAddedCities = .init(DefaultCity.ids)
    }
    
    // MARK:- Private
    
    // MARK: Properties
    
    private let controller: CurrentWeatherControlling
    private let builderProvider: () -> WeatherFeedBuilding
    private let router: WeatherFeedRouting
    private var userAddedCities: Set<String> = []
}

//MARK: - Conformance

// MARK: WeatherFeedViewModelType

extension WeatherFeedViewModel: WeatherFeedViewModelType {
    
    func displayDidAppear() {
        
        self.display?.set(title: "openweathermap.org")
        
        display?.set(
            refreshItem: WeatherRefreshItem(
                refreshInterval: 30.0,
                shouldRepeat: true,
                action: { [weak self] in
                    guard let self = self else { return }
                    self.fetchCurrentWeather(for: self.userAddedCities)
                }
            )
        )
        
        display?.set(
            rightBarButtonItem: NavigationButtonItem(
                systemItem: .add,
                action: addButtonAction()
            )
        )
        fetchCurrentWeather(for: self.userAddedCities)
    }
}

// MARK:- Helpers

// MARK: Network

private extension WeatherFeedViewModel {
    
    func fetchCurrentWeather(for cities: Set<String>) {
        controller.weather(
            for: Array(cities)
        ) { [weak self] result in
            guard let self = self else { return }
            
            self.handleResult(result)
        }
    }
    
    func handleResult(_ result: Result<CurrentWeatherResponse, WeatherError>) {
        switch result {
        case .success(let weatherResponse):
            buildSections(weatherResponse)
        case .failure(let weatherError):
            handleFailure(weatherError)
        }
    }
}

// MARK: UI Builders

private extension WeatherFeedViewModel {
    
    func buildSections(_ response: CurrentWeatherResponse) {
        let sections = builderProvider()
            .addWeatherFeed(response, action: cellAction())
            .build()
        
        display?.set(sections: sections)
    }
    
    func buildError(_ errorText: String) {
        let sections = builderProvider()
            .addErrorView(errorText)
            .build()
        
        display?.set(sections: sections)
    }
    
    func handleFailure(_ error: WeatherError) {
        switch error {
        case .emptyList:
            buildError("There is no weather information at this moment.")
        case .network:
            buildError("Check your internet connection and try again!")
        case .custom(let errorDescription):
            buildError(errorDescription)
        }
    }
}

// MARK: Navigation

private extension WeatherFeedViewModel {
    
    func addButtonAction() -> (() -> Void) {
        return { [weak self] in
            guard let self = self else { return }
            self.router.showAddCityScreen(addCityHandler: self.addCityHandler())
        }
    }
    
    func cellAction() -> ((Weather) -> (() -> Void)) {
        return { [weak self] weather in
            guard let self = self else { return {} }
            
            return {
                self.router.showWeatherDetailScreen(with: weather)
            }
        }
    }
    
    func addCityHandler() -> (CityIdentifier) -> Void {
        return { [weak self] cityIdentifier in
            guard let self = self else { return }
            
            self.userAddedCities = .init(["\(cityIdentifier)"] + self.userAddedCities)
            self.fetchCurrentWeather(for: self.userAddedCities)
        }
    }
}

// MARK: Constants

private extension WeatherFeedViewModel {
    enum DefaultCity {
        static let ids = ["4163971", "2147714", "2174003"]
    }
}
