import Foundation

protocol WeatherFeedBuilding {
    func build() -> [TableViewSectionItem]
    func addWeatherFeed(_ feed: CurrentWeatherResponse, action: @escaping (Weather) -> (() -> Void)) -> Self
    func addErrorView(_ errorText: String) -> Self
}

final class WeatherFeedBuilder {
    
    // MARK: Builder Output
    
    private var sections: [TableViewSectionItem] = []
}

// MARK: - Conformance

// MARK: WeatherFeedBuilding

extension WeatherFeedBuilder: WeatherFeedBuilding {
    func build() -> [TableViewSectionItem] {
        return sections
    }
    
    @discardableResult func addWeatherFeed(
        _ feed: CurrentWeatherResponse,
        action: @escaping (Weather) -> (() -> Void)
    ) -> Self {
        
        guard let feedList = feed.list else {
            return self
        }

        let items = feedList.map{
            WeatherFeedViewItem(
                cityText: $0.name ?? "Unknown",
                temperatureText: "\($0.main?.temperature ?? 0.0)°",
                temperatureRange: "\($0.main?.temperatureMin ?? 0.0)° - \($0.main?.temperatureMax ?? 0.0)°",
                action: action($0)
            )
        }
        
        sections.append(
            TableViewSectionItem(
                header: WeatherFeedHeaderFooterItem(title: "Weather Today"),
                items: items
            )
        )
        return self
    }
    
    @discardableResult func addErrorView(
        _ errorText: String
    ) -> Self {
            
        sections.append(
            TableViewSectionItem(
                items: [ListErrorViewItem(errorText: errorText)]
            )
        )
        
        return self
    }
}
