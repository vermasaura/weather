import Foundation
import UIKit

final class CityListCell: UITableViewCell, Reusable {
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var title: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        title.text = ""
    }
    
    // MARK: - View Configuration
    
    func configure(with cellItem: CityListViewItem) {
        title.text = cellItem.cityText
    }
}
