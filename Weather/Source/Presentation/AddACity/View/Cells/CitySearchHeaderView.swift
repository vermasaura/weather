import Foundation
import UIKit

final class CitySearchHeaderView: UITableViewHeaderFooterView, Reusable {
    
    @IBOutlet private weak var searchBar: UISearchBar! {
        didSet {
            searchBar.becomeFirstResponder()
        }
    }
    private var action: ((String) -> Void)?
    
    func configure(with item: CitySearchHeaderViewItem) {
        searchBar.placeholder = item.placeholder
        action = item.action
    }
}

// MARK: - Conformance

// MARK: UISearchBarDelegate

extension CitySearchHeaderView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 2 {
            action?(searchText)
        }
    }
}
