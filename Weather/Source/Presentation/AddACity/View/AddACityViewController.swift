import Foundation
import UIKit

final class AddACityViewController: UIViewController {
    
    // MARK: IBOutlet
    
    @IBOutlet private var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: String(describing: CityListCell.self),
                                     bundle: Bundle.main),
                               forCellReuseIdentifier: String(describing: CityListCell.self))
            tableView.register(UINib(nibName: String(describing: ListErrorCell.self),
                                     bundle: Bundle.main),
                               forCellReuseIdentifier: String(describing: ListErrorCell.self))
            tableView.register(UINib(nibName: String(describing: CitySearchHeaderView.self),
                                     bundle: Bundle.main),
                               forHeaderFooterViewReuseIdentifier: String(describing: CitySearchHeaderView.self))
        }
    }
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Initialisation
    
    init(
        viewModel: AddACityViewModelType
    ) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: AddACityViewController.self), bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("use init(viewModel:) instead")
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.startAnimating()
        viewModel.displayDidAppear()
    }
    
    // MARK: - Private
    
    // MARK: Properties
    
    private let viewModel: AddACityViewModelType
    private var leftBarButtonAction: (() -> Void)?
    private var sections: [TableViewSectionItem] = []
}

// MARK: - Helpers

private extension AddACityViewController {
    
    @objc func navigationBarButtonAction() {
        leftBarButtonAction?()
    }
    
}

// MARK: - Conformance

// MARK: UITableViewDataSource

extension AddACityViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return sections[indexPath.section]
            .items[indexPath.row]
            .extractCell(from: tableView, for: indexPath)
    }
}

// MARK: UITableViewDelegate

extension AddACityViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        sections[indexPath.section]
            .items[indexPath.row]
            .action?()
    }
    
    // MARK: Header Configuration
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard sections[section].header != nil else {
            return .leastNormalMagnitude
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return sections[section]
            .header?
            .extractView(from: tableView, for: section)
    }
    
    // MARK: Footer Configuration
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard sections[section].footer != nil else {
            return .leastNormalMagnitude
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return sections[section]
            .footer?
            .extractView(from: tableView, for: section)
    }
}

// MARK: - Conformance

// MARK: AddACityDisplaying

extension AddACityViewController: AddACityDisplaying {
    func set(title: String?) {
        self.title = title
    }
    
    func set(leftBarButtonItem: NavigationButtonItem) {
        navigationItem.setLeftBarButton(
            UIBarButtonItem.init(
                barButtonSystemItem: leftBarButtonItem.systemItem,
                target: self,
                action: #selector(navigationBarButtonAction)
            ),
            animated: true
        )
        leftBarButtonAction = leftBarButtonItem.action
    }
    
    func set(sections: [TableViewSectionItem]) {
        activityIndicator.stopAnimating()
        self.sections = sections
        tableView.reloadData()
    }
}
