import Foundation
import UIKit

struct CityListViewItem {
    let id: CityIdentifier
    let cityText: String
    let action: (() -> Void)?
}

extension CityListViewItem: CellDisplayable {
    func extractCell(from tableView: UITableView, for indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CityListCell.identifier) as? CityListCell else {
            return UITableViewCell()
        }
        cell.configure(with: self)
        return cell
    }
}

typealias CityIdentifier = UInt
