import Foundation
import UIKit

struct CitySearchHeaderViewItem {
    let placeholder: String
    let action: (String) -> Void
}

extension CitySearchHeaderViewItem: HeaderFooterDisplayable {
    func extractView(from tableView: UITableView, for section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CitySearchHeaderView.identifier) as? CitySearchHeaderView else {
            return nil
        }
        
        headerView.configure(with: self)
        return headerView
    }
}
