import Foundation

protocol AddACityBuilding {
    func build() -> [TableViewSectionItem]
    func addSearchBar(_ placeholder: String, action: @escaping (String) -> Void) -> Self
    func addCitiesList(_ cities: [City], action: @escaping (CityIdentifier) -> (() -> Void)) -> Self
    func addErrorView(_ errorText: String) -> Self
}

final class AddACityBuilder {
    
    // MARK: Builder Output
    
    private var sections: [TableViewSectionItem] = []
}

// MARK: - Conformance

// MARK: AddACityBuilding

extension AddACityBuilder: AddACityBuilding {
    func build() -> [TableViewSectionItem] {
        return sections
    }
    
    @discardableResult func addSearchBar(
        _ placeholder: String,
        action: @escaping (String) -> Void
    ) -> Self {

        sections.append(
            TableViewSectionItem(
                header: CitySearchHeaderViewItem(placeholder: placeholder, action: action),
                items: []
            )
        )
        return self
    }
    
    @discardableResult func addCitiesList(
        _ cities: [City],
        action: @escaping (CityIdentifier) -> (() -> Void)
    ) -> Self {
        
        let items = cities
            .map{
                return CityListViewItem(
                    id: $0.id,
                    cityText: "\($0.name), \($0.country)",
                    action: action($0.id)
                )
            }
        
        sections.append(
            TableViewSectionItem(
                items: items
            )
        )
        return self
    }
    
    @discardableResult func addErrorView(
        _ errorText: String
    ) -> Self {
            
        sections.append(
            TableViewSectionItem(
                items: [ListErrorViewItem(errorText: errorText)]
            )
        )
        
        return self
    }
}
