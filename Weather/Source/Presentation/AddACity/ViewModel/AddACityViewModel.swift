import Foundation

protocol AddACityDisplaying: AnyObject {
    func set(title: String?)
    func set(leftBarButtonItem: NavigationButtonItem)
    func set(sections: [TableViewSectionItem])
}

protocol AddACityViewModelType {
    func displayDidAppear()
}

final class AddACityViewModel {
    
    // MARK:- Public
    
    weak var display: AddACityDisplaying?
    
    // MARK: - Initialising
    
    init(router: AddACityRouter,
         builderProvider: @escaping () -> AddACityBuilding = { AddACityBuilder() },
         controller: SearchCityControlling = SearchCityController()) {
        self.router = router
        self.builderProvider = builderProvider
        self.controller = controller
    }
    
    // MARK: - Private
    
    // MARK: Properties
    
    private let router: AddACityRouting
    private let builderProvider: () -> AddACityBuilding
    private let controller: SearchCityControlling
}

// MARK:- Conformance

// MARK: AddACityViewModelType

extension AddACityViewModel: AddACityViewModelType {
    func displayDidAppear() {
        
        display?.set(title: "Loading city database...")
        display?.set(
            leftBarButtonItem: NavigationButtonItem(
                systemItem: .cancel,
                action: { [weak self] in
                    
                    guard let self = self else { return }
                    self.router.goBack()
                }
            )
        )
        
        controller.openDatabase { [weak self] in
            guard let self = self else { return }
            self.buildSearchBar()
            self.display?.set(title: "Add City")
        }
    }
}

// MARK: - Helpers

// MARK: Network

private extension AddACityViewModel {
    func searchCity(by searchText: String) {
        controller.cities(
            by: searchText) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let cities):
                self.buildSections(with: cities)
            case .failure(let cityError):
                self.handleError(cityError)
            }
        }
    }
    
    func handleError(_ error: SearchCityError) {
        switch error {
        case .emptyList:
            buildError("No Cities found by this name. Try modifying your search.")
        case .custom(let errorDescription):
            buildError(errorDescription)
        }
    }
}

// MARK: UI Builders

private extension AddACityViewModel {
    
    func buildSections(with cities: [City]) {
        let sections = builderProvider()
            .addSearchBar("Search by city name", action: searchAction())
            .addCitiesList(cities, action: cellAction())
            .build()
        
        display?.set(sections: sections)
    }
    
    func buildSearchBar() {
        let sections = builderProvider()
            .addSearchBar("Search by city name", action: searchAction())
            .build()
        
        display?.set(sections: sections)
    }
    
    func buildError(_ errorText: String) {
        let sections = builderProvider()
            .addSearchBar("Search by city name", action: searchAction())
            .addErrorView(errorText)
            .build()
        
        display?.set(sections: sections)
    }
}

// MARK: Actions

private extension AddACityViewModel {
    func searchAction() -> ((String) -> Void) {
        return { [weak self] searchText in
            guard let self = self else { return }
            self.searchCity(by: searchText)
        }
    }
    
    func cellAction() -> ((CityIdentifier) -> (() -> Void)) {
        return { [weak self] cityIdentifier in
            guard let self = self else { return {} }
            
            return {
                self.router.addCity(with: cityIdentifier)
            }
        }
    }
}
