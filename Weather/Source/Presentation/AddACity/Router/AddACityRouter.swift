import Foundation
import UIKit

protocol AddACityRouting {
    func goBack()
    func addCity(with identifier: CityIdentifier)
}

final class AddACityRouter {
    
    // MARK: Public
    
    weak var display: UINavigationController?
    
    // MARK: Initialising
    
    init(addCityHandler: @escaping (CityIdentifier) -> Void) {
        self.addCityHandler = addCityHandler
    }
    
    private let addCityHandler: (CityIdentifier) -> Void
}

// MARK: - Conformance

// MARK: AddACityRouting

extension AddACityRouter: AddACityRouting {
    func goBack() {
        display?.dismiss(animated: true)
    }
    
    func addCity(with identifier: CityIdentifier) {
        addCityHandler(identifier)
        display?.dismiss(animated: true)
    }
}

