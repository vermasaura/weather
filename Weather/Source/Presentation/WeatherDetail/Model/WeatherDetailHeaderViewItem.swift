import Foundation
import UIKit

struct WeatherDetailHeaderViewItem {
    let primaryText: String
    let secondaryText: String
    
    var accessibilityText: String {
        return "\(primaryText), \(secondaryText)"
    }
}

extension WeatherDetailHeaderViewItem: HeaderFooterDisplayable {
    func extractView(from tableView: UITableView, for section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: WeatherDetailHeaderView.identifier) as? WeatherDetailHeaderView else {
            return nil
        }
        
        headerView.configure(with: self)
        return headerView
    }
}
