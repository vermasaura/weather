import Foundation
import UIKit

struct WeatherDetailViewItem {
    let primaryText: String
    let secondaryText: String
    
    var accessibilityText: String {
        return "\(primaryText), \(secondaryText)"
    }
}

extension WeatherDetailViewItem: CellDisplayable {
    func extractCell(from tableView: UITableView, for indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WeatherDetailCell.identifier) as? WeatherDetailCell else {
            return UITableViewCell()
        }
        cell.configure(with: self)
        return cell
    }
}


