import Foundation

protocol WeatherDetailBuilding {
    func build() -> [TableViewSectionItem]
    func addWeatherSummary(_ weather: Weather) -> Self
}

final class WeatherDetailBuilder {
    
    // MARK: Builder Output
    
    private var sections: [TableViewSectionItem] = []
}

// MARK: - Conformance

// MARK: WeatherDetailBuilding

extension WeatherDetailBuilder: WeatherDetailBuilding {
    func build() -> [TableViewSectionItem] {
        return sections
    }
    
    func addWeatherSummary(_ weather: Weather) -> Self {
        
        guard let main = weather.main else {
            return self
        }

        let header = WeatherDetailHeaderViewItem(
            primaryText: weather.name ?? "Unknown",
            secondaryText: "Today: It's \(main.temperature)°; the maximum today was forcast as \(main.temperatureMax)°"
        )
        
        let items = [
            WeatherDetailViewItem(primaryText: "MIN", secondaryText: "\(main.temperatureMin)°"),
            WeatherDetailViewItem(primaryText: "MAX", secondaryText: "\(main.temperatureMax)°"),
            WeatherDetailViewItem(primaryText: "FEELS LIKE", secondaryText: "\(main.feelsLike)°"),
            WeatherDetailViewItem(primaryText: "HUMIDITY", secondaryText: "\(main.humidity)%")
        ]
        
        sections.append(
            TableViewSectionItem(
                header: header,
                items: items
            )
        )
        
        return self
    }
}
