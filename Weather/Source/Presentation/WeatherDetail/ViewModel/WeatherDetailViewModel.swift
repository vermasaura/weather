protocol WeatherDetailDisplaying: AnyObject {
    func set(title: String?)
    func set(sections: [TableViewSectionItem])
}

protocol WeatherDetailViewModelType {
    func displayDidAppear()
}

final class WeatherDetailViewModel {
    
    // MARK:- Public
    weak var display: WeatherDetailDisplaying?
    
    // MARK:- Initialising
    
    init(
        weather: Weather,
        builderProvider: @escaping () -> WeatherDetailBuilding = { WeatherDetailBuilder() },
        router: WeatherDetailRouting
    ) {
        self.weather = weather
        self.builderProvider = builderProvider
        self.router = router
    }
    
    // MARK:- Private
    
    // MARK: Properties
    
    private let weather: Weather
    private let builderProvider: () -> WeatherDetailBuilding
    private let router: WeatherDetailRouting
}

//MARK: - Conformance

// MARK: WeatherDetailViewModelType

extension WeatherDetailViewModel: WeatherDetailViewModelType {
    
    func displayDidAppear() {
        display?.set(title: "Weather Summary")
        
        let sections = builderProvider()
            .addWeatherSummary(weather)
            .build()
        
        display?.set(sections: sections)
    }
}
