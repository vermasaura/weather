import Foundation
import UIKit

protocol WeatherDetailRouting {
    func goBack()
}

final class WeatherDetailRouter {
    
    weak var display: UINavigationController?
}

// MARK: - Conformance

// MARK: WeatherDetailRouting

extension WeatherDetailRouter: WeatherDetailRouting {
    func goBack() {
        // TODO:
    }
}
