import Foundation
import UIKit

final class WeatherDetailCell: UITableViewCell, Reusable {
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var primaryLabel: UILabel!
    @IBOutlet private weak var secondaryLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        primaryLabel.text = ""
        secondaryLabel.text = ""
    }
    
    // MARK: - View Configuration
    
    func configure(with cellItem: WeatherDetailViewItem) {
        primaryLabel.text = cellItem.primaryText
        secondaryLabel.text = cellItem.secondaryText
        configureAccessibility(cellItem.accessibilityText)
    }
}

// MARK: Accessibility

private extension WeatherDetailCell {
    func configureAccessibility(_ accessibilityText: String) {
        primaryLabel.isAccessibilityElement = false
        secondaryLabel.isAccessibilityElement = false
        isAccessibilityElement = true
        accessibilityLabel = accessibilityText
    }
}

