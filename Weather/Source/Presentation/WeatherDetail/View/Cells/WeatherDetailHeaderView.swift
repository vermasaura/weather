import Foundation
import UIKit

final class WeatherDetailHeaderView: UITableViewHeaderFooterView, Reusable {
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var primaryLabel: UILabel!
    @IBOutlet private weak var secondaryLabel: UILabel!
    
    func configure(with item: WeatherDetailHeaderViewItem) {
        primaryLabel.text = item.primaryText
        secondaryLabel.text = item.secondaryText
    }
}
