import Foundation
import UIKit

final class WeatherDetailViewController: UIViewController {
    
    // MARK: IBOutlet
    
    @IBOutlet private var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: String(describing: WeatherDetailCell.self),
                                     bundle: Bundle.main),
                               forCellReuseIdentifier: String(describing: WeatherDetailCell.self))
            tableView.register(UINib(nibName: String(describing: WeatherDetailHeaderView.self),
                                     bundle: Bundle.main),
                               forHeaderFooterViewReuseIdentifier: String(describing: WeatherDetailHeaderView.self))
        }
    }
    
    // MARK: - Initialisation
    
    init(
        viewModel: WeatherDetailViewModelType
    ) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: WeatherDetailViewController.self), bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("use init(viewModel:) instead")
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.displayDidAppear()
    }
    
    // MARK: Private
    
    // MARK: Properties
    
    private let viewModel: WeatherDetailViewModelType
    private var sections: [TableViewSectionItem] = []
}

// MARK: - Conformance

// MARK: UITableViewDataSource

extension WeatherDetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return sections[indexPath.section]
            .items[indexPath.row]
            .extractCell(from: tableView, for: indexPath)
    }
}

// MARK: UITableViewDelegate

extension WeatherDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        sections[indexPath.section]
            .items[indexPath.row]
            .action?()
    }
    
    // MARK: Header Configuration
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard sections[section].header != nil else {
            return .leastNormalMagnitude
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return sections[section]
            .header?
            .extractView(from: tableView, for: section)
    }
    
    // MARK: Footer Configuration
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard sections[section].footer != nil else {
            return .leastNormalMagnitude
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return sections[section]
            .footer?
            .extractView(from: tableView, for: section)
    }
}

// MARK: WeatherDetailDisplaying

extension WeatherDetailViewController: WeatherDetailDisplaying {
    func set(title: String?) {
        self.title = title
    }

    func set(sections: [TableViewSectionItem]) {
        self.sections = sections
        tableView.reloadData()
    }
}
