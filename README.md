# Weather-iOS

**Table of Contents**

- [Getting Started](#getting-started)
- [Architecture, Design Patterns and Other Documentation](#architecture-design-patterns-and-other-documentation)

## Getting Started

## Problem
- Use a UITableViewController to display weather information of Sydney, Melbourne and Brisbane as start.

- Provide a way to add more cities using another modal view controller which includes a search functionality to find a city by name.

- City IDs
-- Sydney, Melbourne and Brisbane are: 4163971, 2147714, 2174003
-- More city can be found from the following link:
http://bulk.openweathermap.org/sample

-  Each cell should display at least two pieces of info: Name of city on the left, temperature on the right.

- Get real time weather information using https://openweathermap.org/current - You
can register and get your API key for free.

- A sample request to get weather info for one city:
o http://api.openweathermap.org/data/2.5/weather?id=4163971&units=metri
c&APPID=your_registered_API_key

- Weather should be automatically updated periodically.

### Environmental setup
- macOS version: Catalina 10.15.7
- Xcode version: 12.1
- Swift version: 5

## Architecture, Design Patterns and Other Documentation

The project is built using clean architecture. Entities(Models) -> Use Cases(Controllers) -> Presentation(View Models/Builders) -> Views(ViewController/Cells etc)

All the 3 layers Presentation, Business and Network is built using protocol/ interface oriented programming. Dependencies are independent and replaceable which helps the code 100% testable.

The project has been made using Test-driven-development methodology where-in User Stories are mapped to unit tests.
Unit tests are only written for `CurrentWeatherController`, `WeatherFeedViewModel` and `WeatherFeedBuilder`.

Unit tests also uses protocol oriented programming to have all the test scenarios defined as rules on the top of XCTestCase class. The unit tests enjoys the use of dependency injection to inject stubs / mocks in the class being tested.

The properties in the Models (eg: `Weather.swift`) and Display Models (eg: `WeatherFeedViewItem.swift`) are immutable to avoid side effects state changes.

Ideally, views / view controller doesn't know about the models. Hence, display models are being used to map the data from models to UI.
Routers are used to keep the presentation logic out of the view controller.
Xibs are used instead of storyboard so that view controllers dependencies can be injected in the `init`.

The following concepts are used throughout the codebase:

- [Protocol-Oriented with BDD](https://dzone.com/articles/introducing-protocol-oriented-bdd-in-swift-for-ios)
- [MVVM + Router](https://medium.com/commencis/routing-with-mvvm-on-ios-f22d021ad2b2)
- [Dependency Injection](https://cocoacasts.com/nuts-and-bolts-of-dependency-injection-in-swift)
